<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('products', function (Blueprint $table){
            $table->string('image', 100)->change();
            $table->string('image_size',100)->change();
            $table->string('image_application',100)->change();
            $table->string('data_sheet',100)->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('products', function (Blueprint $table){
            $table->string('image', 24)->change();
            $table->string('image_size',24)->change();
            $table->string('image_application',24)->change();
            $table->string('data_sheet',24)->change();
        });
    }
}
