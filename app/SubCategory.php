<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SubCategory extends Model
{
    protected $table = 'product_sub_categories';

    public function category(){
        return $this->belongsTo('App\Category');
    }

    public function products(){
        return $this->hasMany('App\Product');
    }
}
