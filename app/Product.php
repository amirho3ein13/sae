<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $table = 'products';

    public function sub_category(){
        return $this->belongsTo('App\SubCategory');
    }
}
