<?php

namespace App\Http\Controllers;

use App\Category;
use App\SubCategory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Log;

class SubCategoryController extends Controller
{
    public function add(){
        $sub_cat = new SubCategory;
        $sub_cat->name = Input::get('name');
        $sub_cat->category_id = Category::where('name', 'Like', Input::get('category_name'))->first()->id;
        $sub_cat->save();
    }

    public function get_product($id){
        $cats = Category::all();
        $products = array();
        foreach ($cats as $cat){
            $cat->sub_categories;
            foreach ($cat->sub_categories as $sub) {
                if ($sub->id == $id) {
                    array_push($products, $sub->products);
                    $_id = $cat->id;
                    break;
                }
            }
        }
//        Log::info($cats);
        return view('products')->with('data', ['cats' => $cats, 'id' => $_id, 'products' => $products]);
//        return ['cats' => $cats, 'id' => $_id];
    }
}
