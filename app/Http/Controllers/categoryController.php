<?php

namespace App\Http\Controllers;

use App\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Log;

class categoryController extends Controller
{
    public function add(){
        $cat = new Category;
        $cat->name = Input::get('name');
        $cat->save();
    }
    public function get_categories(){
        return view('products')->with('category', Category::all());
    }
    public function get_product($id){
        $cats = Category::all();
        $products = array();
        foreach ($cats as $cat){
            $cat->sub_categories;
            if ($cat->id == $id) {
                foreach ($cat->sub_categories as $sub) {
                    array_push($products, $sub->products);
                    break;
                }
            }
        }
        return view('products')->with('data', ['cats' => $cats, 'id' => $id, 'products' => $products]);
//        return ['cats' => $cats, 'id' => $id, 'products' => $products];
    }
}
