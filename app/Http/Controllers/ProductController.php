<?php

namespace App\Http\Controllers;

use App\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

class ProductController extends Controller
{
    public function add(Request $request){
        $product = new Product;
        $product->name = $request->get('name');
        $product->sub_category_id = $request->get('number');
        $product->image = $request->file('image')->store('product_files/images/main_images');
        $product->image_size = $request->file('size')->store('product_files/images/size_images');
        $product->image_application = $request->file('app')->store('product_files/images/application_images');
        $product->data_sheet = $request->file('datasheet')->store('product_files/datasheets');
        $product->save();
        redirect('product/add');
    }
}
