<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
});

Route::get('add/category', 'categoryController@add');

Route::get('add/sub_category', 'SubCategoryController@add');

Route::get('category/{id}', 'categoryController@get_product');

Route::get('sub_category/{id}', 'SubCategoryController@get_product');

Route::get('product/add', function (){
    return view('add_product');
});

Route::post('add_product', 'ProductController@add');