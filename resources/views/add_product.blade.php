@extends('layouts.app_material')

@section('content')
    {!! Form::open(['url' => 'add_product', 'method' => 'post', 'files' => true]) !!}
    {!! Form::token() !!}
    image:
    {!! Form::file('image') !!}<br>
    size:
    {!! Form::file('size') !!}<br>
    app:
    {!! Form::file('app') !!}<br>
    datasheet:
    {!! Form::file('datasheet') !!}<br>
    name:
    {!! Form::text('name') !!}<br>
    sub_cat:
    {!! Form::number('number') !!}<br>
    {!! Form::submit('ok') !!}
    {!! Form::close() !!}
@endsection

@section('script')

@endsection