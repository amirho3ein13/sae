@extends('layouts.app_material')

@section('content')
    <div class="row">
        <div class="col s12 m4 l4">
            @foreach($data['products'] as $product)
                @foreach($product as $p)
                    <div class="card hoverable">
                        <div class="card-image">
                            <img src="{{asset('storage/'.$p['image'])}}">
                        </div>
                        <div class="card-content">
                            <h5>{{ $p['name'] }}</h5>
                            <p>shit e sag</p>
                        </div>
                        <div class="card-action">
                            <a href="/product/{{$p['id']}}" class="right-align">More Info</a>
                        </div>
                    </div>
                @endforeach
            @endforeach
        </div>
        <div class="col s12 m3 l3 offset-m5 offset-l5">
            <ul class="collapsible" data-collapsible="expandable">
                @foreach($data['cats'] as $cat)
                    <li>
                        <div class="collapsible-header" id="{{ $cat['id'] }}">{{$cat['name']}}</div>
                        <div class="collapsible-body">
                            <div class="section">
                                @foreach($cat['sub_categories'] as $sub)
                                    <a href="/sub_category/{{ $sub['id'] }}"><p>{{$sub['name']}}</p></a>
                                    <div class="divider"></div>
                                @endforeach
                            </div>
                        </div>
                    </li>
                @endforeach
            </ul>
        </div>
    </div>
@endsection

@section('script')
    <script>
        $(document).ready(function() {
            $('#{{ $data['id'] }}').click()
        })
    </script>
@endsection