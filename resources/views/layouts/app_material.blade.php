<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Styles -->
    <style href="{{ asset('css/app.css') }}" rel="stylesheet"></style>
    {{--<style href="{{ asset('css/bootstrap-rtl.min.css') }}" rel="stylesheet"></style>--}}
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/css/materialize.min.css">
    {{--<link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">--}}
    {{--<style href="{{ asset('css/mystyle.css') }}" rel="stylesheet"></style>--}}
    @yield('style')

</head>
<body>
<div id="app">
    <ul id="dropdown1" class="dropdown-content">
        {{--@foreach($category_dropdown as $cat)--}}
            {{--<li><a href="category/{{$cat['id']}}">{{$cat['name']}}</a></li>--}}
        {{--@endforeach--}}
        <li><a href="/category/1" style="color: #0c0c0c">POE Switch</a></li><li class="divider"></li>
        <li><a href="/category/3" style="color: #0c0c0c">POE Injector</a></li><li class="divider"></li>
        <li><a href="/category/4" style="color: #0c0c0c">PDH</a></li><li class="divider"></li>
        <li><a href="/category/5" style="color: #0c0c0c">Industrial POE Switch</a></li><li class="divider"></li>
        <li><a href="/category/6" style="color: #0c0c0c">Media Converter</a></li><li class="divider"></li>
        <li><a href="/category/7" style="color: #0c0c0c">Video Converter</a></li><li class="divider"></li>
        <li><a href="/category/8" style="color: #0c0c0c">Fiber Optic Converter</a></li><li class="divider"></li>
        <li><a href="/category/9" style="color: #0c0c0c">Passive Fiber Optic</a></li><li class="divider"></li>
        <li><a href="/category/10" style="color: #0c0c0c">Fiber Optic Switch</a></li><li class="divider"></li>
        <li><a href="/category/11" style="color: #0c0c0c">Network Media Converter</a></li><li class="divider"></li>

    </ul>
    <nav class="deep-purple darken-3">
        <div class="nav-wrapper">
            <a href="/" class="brand-logo"><img src="{{asset('0_bf582b7f5d5a1603f145923c3ba027e0.png')}}" alt="SAE" style="width: 90px"></a>
            <ul class="right hide-on-med-and-down">
                <li><a href="/">Home</a></li>
                <!-- Dropdown Trigger -->
                <li><a class="dropdown-button" href="" data-activates="dropdown1">Categories</a></li>
                <li><a href="#">Contact Us</a></li>
            </ul>
        </div>
    </nav>

    @yield('content')
</div>
<footer class="page-footer deep-purple darken-3">
    <div class="container">
        <div class="row">
            <div class="col l6 s12">
                <h5 class="white-text">Get In Touch</h5>
                <p class="grey-text text-lighten-4"><a class="white-text" href="mailto:sae@sae-net.com">sae@sae-net.com</a></p>
                <p class="grey-text text-lighten-4">(+98) 3116639191</p>
            </div>
            <div class="col l4 offset-l2 s12">
                <h5 class="white-text">Categories</h5>
                <ul>
                    <li><a class="grey-text text-lighten-3" href="category/1">POE Switch</a></li>
                    <li><a class="grey-text text-lighten-3" href="category/3">POE Injector</a></li>
                    <li><a class="grey-text text-lighten-3" href="category/4">PDH</a></li>
                    <li><a class="grey-text text-lighten-3" href="category/5">Industrial POE Switch</a></li>
                    <li><a class="grey-text text-lighten-3" href="category/6">Media Converter</a></li>
                    <li><a class="grey-text text-lighten-3" href="category/7">Video Converter</a></li>
                    <li><a class="grey-text text-lighten-3" href="category/8">Fiber Optic Converter</a></li>
                    <li><a class="grey-text text-lighten-3" href="category/9">Passive Fiber Optic</a></li>
                    <li><a class="grey-text text-lighten-3" href="category/10">Fiber Optic Switch</a></li>
                    <li><a class="grey-text text-lighten-3" href="category/11">Network Media Converter</a></li>
                </ul>
            </div>
        </div>
    </div>
    <div class="footer-copyright">
        <div class="container">
            © 2017. All rights reserved.
            {{--<a class="grey-text text-lighten-4 right" href="#!">More Links</a>--}}
        </div>
    </div>
</footer>
<!-- Scripts -->
<script src="{{ asset('js/app.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/js/materialize.min.js"></script>
<script>
    $( document ).ready(function(){
        $(".dropdown-button").dropdown({
            hover: true,
            constrainWidth: false,
            belowOrigin: true
        });
    })
</script>
@yield('script')

</body>
</html>
